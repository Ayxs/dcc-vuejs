import Activity from "@/models/Activity";

export default class ActivityService {
    GetRandomActivity(): Promise<Activity>{
        return fetch('https://www.boredapi.com/api/activity/')
            .then(response => {
                if (response.ok){
                    return response.json();
                }
            })
            .then(data => {
                return new Activity(data.activity, data.type, data.participants, data.price, data.link, data.key, data.accessibility);
            });
    }
}
