export default class Activity {
    public Activity: string;
    public Type: string;
    public Participants: number;
    public Price: number;
    public Link: string;
    public Key: string;
    public Accessibility: number;

    constructor(activity: string, type: string, participants: number, price: number, link: string, key: string, accessibility: number) {
        this.Activity = activity;
        this.Type = type;
        this.Participants = participants;
        this.Price = price;
        this.Link = link;
        this.Key = key;
        this.Accessibility = accessibility;
    }
}
